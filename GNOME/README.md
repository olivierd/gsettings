# How to use it

## 1. Create a profile `user`

```sh
mkdir -p /etc/dconf/profile

cat /etc/dconf/profile/user
user-db:user
system-db:local
```

**/!\ Not working anymore for Debian** for the GNOME Display Manager (gdm):

```sh
cat /etc/dconf/profile/gdm
user-db:user
system-db:gdm
file-db:/usr/share/gdm/greeter-dconf-defaults
```

On Debian, put `gdm-debian/91-local-settings` in `/usr/share/gdm/dconf/`, and regenerate `/var/lib/gdm3/greeter-dconf-defaults`.

```
cp 91-local-settings /usr/share/gdm/dconf/

dconf compile /var/lib/gdm3/greeter-dconf-defaults /usr/share/gdm/dconf
```

## 2. Create keyfile settings

```sh
mkdir -p /etc/dconf/db/local.d
cp 00-gnome-settings /etc/dconf/db/local.d/
```

**/!\ Not working anymore for Debian** for the GNOME Display Manager (gdm):

```sh
mkdir /etc/dconf/db/gdm.d
cp gdm.d/login-screen /etc/dconf/db/gdm.d/
```

## 3. Create database

`dconf update` (in **root**)

## Useful commands

Remove previous settings: `dconf reset -f /`

Display current settings: `dconf dump /`

## Note

`deprecated/` folder contains obsolete settings.
