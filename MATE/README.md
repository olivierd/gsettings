# How to use it

## 1. Create a profile `user`

```sh
mkdir -p /etc/dconf/profile

cat /etc/dconf/profile/user
user-db:user
system-db:mate
```

## 2. Create keyfile settings

```sh
mkdir -p /etc/dconf/db/mate.d
cp settings /etc/dconf/db/mate.d/
```

## 3. Create database

`dconf update` (in **root**)

## Useful commands

Remove previous settings: `dconf reset -f /`

Display current settings: `dconf dump /`
